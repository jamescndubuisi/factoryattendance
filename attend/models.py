""" No documentation string yet"""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime as datetime
from django.db import models
import face_recognition
from django.core.validators import ValidationError
from django.http import HttpResponseRedirect

current_month_now = datetime.datetime.now().month
current_year_now = datetime.datetime.now().year
current_day_now = datetime.datetime.now().day
work_time = datetime.datetime(year=current_year_now, day=current_day_now, month=current_month_now, hour=8, minute=0)


def month(current_month):
    """No documentation yet"""
    current_month = int(current_month) - 1
    month_list = ["January", "February", "March", "April", "May", "June"]
    second_half = ["July", "August", "September", "October", "November", "December"]
    month_list = month_list.__add__(second_half)
    current_month = month_list[current_month]
    return current_month

# Create your models here.


class StaffMember(models.Model):
    """documentation does not exist"""
    Gender_choices = (("Male","Male"),("Female", "Female"))
    levels = ((1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, '7'))

    status = (('Active', 'Active'), ('Leave', 'Leave'), ('Retired', 'Retired'),
              ('Fired', 'Fired'), ('Inactive', 'Inactive'))
    first_name = models.CharField(max_length=30, null=True)
    level = models.IntegerField(default=3, choices=levels)
    age = models.IntegerField(default=18)
    image = models.ImageField(upload_to="images", null=True)
    current_status = models.CharField(choices=status, max_length=7, default="Active")
    # face_print = models.TextField(max_length=10000, null=True, blank=True)
    date_registered = models.DateTimeField(auto_now=True)
    gender = models.CharField(max_length=6,choices=Gender_choices, null= True, default="Male")

    def __str__(self):
            return self.first_name


class StaffAttendanceRecord(models.Model):
    """database class for the user entity"""
    full_name = models.ForeignKey(StaffMember, null=True)
    first_name = models.CharField(max_length=20, blank= True)
    signedInTime = models.DateTimeField(auto_now=True)
    current_Month = models.CharField(null=True, max_length=12, blank=True)
    arrival_status = models.CharField(max_length=5, default="late")
    year = models.IntegerField(default=current_year_now)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        """None """
        self.current_Month = month(current_month_now)
        if self.full_name:
            self.first_name=StaffMember.objects.values('first_name').filter(first_name__icontains=self.full_name.first_name).first()['first_name']
        if not self.full_name:
            self.full_name = StaffMember.objects.all().filter(first_name__icontains=self.first_name).first()
        if not self.first_name:
            self.first_name = self.full_name
        if work_time > datetime.datetime.now():
            self.arrival_status = "early"
        elif work_time < datetime.datetime.now():
            self.arrival_status = "late"
        else:
            self.arrival_status = "late"
        super(StaffAttendanceRecord, self).save()

    def __str__(self):
        return self.full_name.first_name
