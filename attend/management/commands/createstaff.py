from django.utils.crypto import get_random_string
from attend.models import StaffMember


from django.core.management import BaseCommand
class Command(BaseCommand):
    help = " awesome"

    def add_arguments(self, parser):
         parser.add_argument('total', type=int , help = "number of users created")

    def handle(self, *args, **options):
        total = options['total']
        for i in range(total):
            new = StaffMember(first_name=get_random_string(),level=4,current_status="leave", age=18)
            new.save()
