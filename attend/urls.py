"""none"""
# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from .views \
    import (homepage,StaffAttendanceRecordView,
            view_staff_list, staff_detail, view_report,
            generate_report, choose_report)
from rest_framework import routers

router = routers.DefaultRouter()
router.register('StaffAttendanceRecord',StaffAttendanceRecordView)

urlpatterns = [
    url(r'^$', homepage),
    # url(r'^generate$', generate_report),
    url(r'^staff_list$', view_staff_list),
    url(r'^staff_profile/([a-z]{3,})$', staff_detail),

    url(r'^overall_report/(?P<month>\w+)/(?P<year>[0-9]{4,})$', view_report, name='overall_report'),
    url(r'^api/', include(router.urls)),

    # url(r'^reports$', PDFView.as_view(template_name='report.html')),
    url(r'^download_report/(?P<month>\w+)/(?P<year>[0-9]{4,})', generate_report),
    # url(r'^check_report', view_report_redirect),
    url(r'^report_choice', choose_report)
    # url(r'^api/$', view=StaffAttendanceRecordView)
]
