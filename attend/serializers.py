from rest_framework import serializers
from .models import StaffAttendanceRecord


class StaffAttendanceRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffAttendanceRecord
        fields = ("first_name",)
