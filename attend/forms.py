# -*- coding: utf-8 -*-
"""none """
from django import forms

import datetime

month_list = (("January", "January"), ("February", "February"), ("March", "March"), ("April", "April"), ("May", "May")
              , ("June", "June"),
              ("July", "July"), ("August", "August"), ("September", "September"), ("October", "October"),
              ("November", "November"), ("December", "December"))

year_list = (("2013",2013),("2014",2014),("2015",2015),("2016",2016),("2017",2017),("2018",2018),)

current_month_now = datetime.datetime.now().month
current_year_now = datetime.datetime.now().year
current_day_now = datetime.datetime.now().day


class ReportGeneration(forms.Form):
    months = forms.ChoiceField(choices=month_list)
    years = forms.ChoiceField(choices=year_list)





