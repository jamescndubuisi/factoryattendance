# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import StaffAttendanceRecord
from .models import StaffMember

# Register your models here.
admin.site.register(StaffMember)
admin.site.register(StaffAttendanceRecord)
admin.site.site_header = 'Attendance system'
admin.site.site_title = 'Attendance system Admin'
admin.site.index_title = 'System Dashboard'




