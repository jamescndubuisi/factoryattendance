# -*- coding: utf-8 -*-
"""none """
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import StaffMember, StaffAttendanceRecord
from django.http import HttpResponse, HttpResponsePermanentRedirect
from rest_framework import viewsets
from .serializers import StaffAttendanceRecordSerializer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from pdfkit import from_url
from forms import ReportGeneration
from .retrain_model import retrain
import datetime
# Create your views here.


def month_index(name):
    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
              "November", "December"]
    index = months.index(name) + 1
    return index



@login_required()
def choose_report(request):
    form = ReportGeneration
    name = 'name'

    if request.method == "POST":
        form = form(request.POST)

        if form.is_valid():
            year = form.cleaned_data['years']
            month = form.cleaned_data['months']
            url = '/overall_report/%s/%d' %(month, int(year))
            return HttpResponsePermanentRedirect(redirect_to=url)

        else:
            pass

    return render(request, "report_choice.html", {"form": form, "name": name})


@login_required()
def today(request):
    name = "Name"
    time = "Time"
    date = "Date"
    data = StaffAttendanceRecord.objects.all().filter(signedInTime__day=datetime.datetime.now().day)
    status = "Status"
    return render(request, "index.html", {"data":data, "name":name,"time": time, "Date": date,"status":status})


@login_required
def homepage(request):
    """No documentation yet """
    name = ""
    if request.user.is_authenticated():
        name = request.user.username
        message = "Since you are a staff, you already know your way around," \
                  " Else head to the ICT department for orientation"
    else:
        message = "You are not a staff, please close the page. Report such error to the ICT department immediately"
    return render(request, "default.html", {"name": name,"message":message})
#
#
# @login_required()
# def generate(request, month, year):
#
#     attendance_list = StaffAttendanceRecord.objects.all()
#     return render(request,'report.html', {"attendance_list": attendance_list})


@login_required()
def view_staff_list(request):
    staff_list = StaffMember.objects.all()
    return render(request, 'staff_list.html', {"staff_list": staff_list})


@login_required()
def staff_detail(request, staff_name):
    staff_details = StaffMember.objects.get(first_name=staff_name)
    return render(request, "staff_profile.html", {"staff_details": staff_details})


@login_required()
def retrain_model(request):
    retrain()
    message = "Training Done"
    return render(request,"default.html",{'message': message})


def view_report(request, month, year):
    attendances = StaffAttendanceRecord.objects.all().filter(current_Month__icontains=month, year= year)
    members = StaffMember.objects.all()
    counts = []
    month_name = month
    month_number = month_index(month)
    year_name = year
    business_days = 0
    for member in members:
        name = member.first_name
        record = attendances.filter(first_name=name).count()
        counts.append(record)
    counts = counts[::-1]
    for i in range(1,32):
        now = datetime.datetime.now()
        holidays = {datetime.date(now.year, 8, 28)} # add more holidays with now.year, month, day,
        try:
            thisdate = datetime.date(int(year), int(month_number), i)
        except ValueError:
            break
        if thisdate.weekday()<5 and thisdate not in holidays:
            business_days +=1

    return render(request, "report.html", {"staffs": members, "records": counts, "month": month_name, "year": year_name,"businessdays":business_days})


@login_required()
def generate_report(request,month,year):
    """Generates report"""
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename = "report.pdf"'
    pdf = from_url("http://localhost:8000/overall_report/{}/{}".format(month,year), False)
    response.write(pdf)
    return response


@permission_classes((AllowAny,))
class StaffAttendanceRecordView(viewsets.ModelViewSet):
    queryset = StaffAttendanceRecord.objects.all()
    serializer_class = StaffAttendanceRecordSerializer


