# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-09-17 02:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('attend', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staffattendancerecord',
            name='full_name',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='attend.StaffMember'),
        ),
    ]
